(C) 1993, 2002 Center for Computer Assisted Research in the Humanities.
ID: {bach/bg/cant/0135/stage2/04/01} [KHM:830216886]
TIMESTAMP: NOV/23/2005 [md5sum:87383d572982d53409a47e4c78c5be57]
04/16/93 S. Rasmussen
WK#:135       MV#:4
Bach Gesellschaft xxviii
Ach Herr, mich armen S\u3nder
Recitative
Alto
0 78 A
Group memberships: sound score
sound: part 1 of 2
score: part 1 of 2
$  K:0   Q:4   T:1/1   C:4   D:Adagio
rest   4        q
A4     4        q     u                    Ich
Bf4    2        e f   d  [     (           bin_
C5     1        s     d  =[                _
D5     1        s     d  ]]    )           _
C5     4        q     d                    von
measure 2
Bf4    2        e f   u  [     (           Seuf-
A4     2        e     u  ]     )           -
rest   2        e
Bf4    2        e     d                    zen
Af4    2        e f   u  [                 m\u3-
G4     2-       e     u  ]     -           -
G4     1        s     u  [[                -
F#4    1        s #   u  =]                -
G4     2        e     u  ]                 -
mdouble 3
A4     2        e     u        +           de,
rest   1        s
D4     1        s     u                    mein
C5     2        e     d                    Geist
A4     2        e     u                    hat
F#4    2        e #   u                    we-
F#4    2        e     u                    der
D5     2        e     d                    Kraft
A4     2        e     u                    noch
measure 4
Bf4    4        q f   d                    Macht,
rest   2        e
Bf4    2        e     d                    weil
Bf4    2        e     d                    ich
G4     2        e     u                    die
Ef4    2        e f   u                    gan-
G4     2        e     u                    ze
measure 5
C#4    4        q #   u                    Nacht,
rest   1        s
A4     1        s     u                    oft
E4     1        s     u                    oh-
F4     1        s     u                    ne
G4     2        e     u                    See-
G4     2        e     u                    len-
Bf4    2        e f   d                    ruh'
A4     2        e     u                    und
measure 6
F4     2        e     u                    Frie-
F4     2        e     u                    de,
rest   1        s
D4     1        s     u                    in
F4     1        s     u                    gro\0/-
A4     1        s     u                    em
D5     2        e     d                    Schwei\0/
rest   1        s
C5     1        s     d                    und
B4     3        e.    d        +           Tr\a3-
C5     1        s     d                    nen
measure 7
C5     2        e     d                    lie-
G4     2        e     u                    ge.
rest   4        q
rest   4        q
rest   2        e
C5     2        e     d                    Ich
measure 8
C5     3        e.    d                    gr\a3-
F#4    1        s #   u                    me
F#4    2        e     u                    mich
A4     2        e     u                    fast
Ef4    4        q f   u                    todt,
rest   2        e
A4     2        e     u                    und
measure 9
A4     2        e     u                    bin
C4     2        e     u                    vor
Ef4    2        e f   u                    Trau-
D4     2        e     u                    ern
gC4    6    @a  e     u        (
S  C1:t50
Bf3    4        q f   u        )           alt,
rest   2        e
D5     2        e     d                    denn
measure 10
D5     2        e     d                    mei-
B4     2        e     d         +          ne
gA4    6        e     u        (
S  C1:t66
G#4    3        e.#   u        )           Angst
A4     1        s     u                    ist
A4     2        e     u  [                 man-
F#4    2        e #   u  =                 -
D#4    3        e.#   u  ]                 -
C5     1        s     d                    nig-
measure 11
A4     4        q     u                    falt!
rest   4        q
rest   8        h
mheavy2
/FINE
a)  Bach Gesellschaft represents this and the following two grace notes as
small hooks preceding the principal note. As Frederick Neumann explains in
&dBOrnamentation in Baroque and Post-Baroque Music&d@, 1978 Princeton U. Press,
p.125-6, n.3, citing BG editor Wilhelm Rust's preface to BG vol. xii, "the
hooks were strictly an editorial device to indicate a little note that was
absent in the autograph score but present in the original performance
parts"; a misleading device, because Bach actually did use the hook symbol,
but exclusively in keyboard works.
/END
@SUPERSEDES: TIMESTAMP: DEC/26/2001 [md5sum:a0be41f80045d3f0f6bd215804953fe5]
@REASON: major archive update
